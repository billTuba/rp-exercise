(ns rp-exercise.helpers
  (:require [clojure.java.io :as io])
  (:import java.io.File))

(defn tmp-file [prefix suffix]
  (->> (io/file (System/getProperty "java.io.tmpdir"))
       (File/createTempFile prefix suffix)))
