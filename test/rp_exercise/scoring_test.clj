(ns rp-exercise.scoring-test
  (:require [clojure.core.async :as a]
            [clojure.test :refer :all]
            [rp-exercise.scoring :as scoring]))

(def default-score -1)

(deftest test-append-score
  (testing "xform appends score to events"
    (let [t      {:A 1
                  :B 2}
          events [{:name "a"    :event-type :A}
                  {:name "b"    :event-type :B}
                  {:name "TEST" :event-type 'uses-default-score}]
          score-default-set
          (comp  (scoring/->xform {:score-table t :default-score  default-score})
                 (map (juxt :name :score)))]

      (is (= #{["a"    1]
               ["b"    2]
               ["TEST" default-score]}
             (transduce score-default-set conj #{} events))))))

(deftest test-score!
  (are [init-state event expected]
       (let [sink (scoring/atomic-sink init-state)]
         (is (= expected
                (scoring/score!  sink event))))

    {}
    {:user-id "bill" :event-type :A :score 1}
    {"bill" {:A 1}}

    {"bill" {:A 1}}
    {:user-id "bill" :event-type :A :score 1}
    {"bill" {:A 2}}

    {"bill" {:A 1 :B 2}}
    {:user-id "bill" :event-type :A :score 1}
    {"bill" {:A 2 :B 2}}))

(deftest test-total

  (are [init-state event user-id expected]
       (let [sink (scoring/atomic-sink init-state)
             _ (if event
                 (scoring/score! sink event))]
         (is (= expected
                (scoring/total sink user-id))))

    {}
    nil
    "fred"
    0

    {}
    {:user-id "bill" :event-type :A :score 1}
    "bill"
    1

    {"bill" {:A 1}}
    {:user-id "bill" :event-type :A :score 1}
    "bill"
    2

    {"bill" {:A 1 :B 2}}
    {:user-id "bill" :event-type :A :score 1}
    "bill"
    4))

(deftest test-async-score!
  (let [sink (scoring/atomic-sink)
        chan (a/to-chan!
              [{:user-id "bill" :event-type :A :score 1}
               {:user-id "fred" :event-type :B :score 2}
               {:user-id "bill" :event-type :A :score 3}
               {:user-id "bill" :event-type :B :score 1}])]
    (scoring/async-score! chan sink)
    (Thread/sleep 100)
    (and
     (is (= {:A 4 :B 1}
            (scoring/-scores sink "bill")))
     (is (=  {:B 2}
             (scoring/-scores sink "fred"))))))





