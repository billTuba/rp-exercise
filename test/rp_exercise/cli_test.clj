(ns rp-exercise.cli-test
  (:require [clojure.java.io :as io]
            [clojure.test :refer :all]
            [rp-exercise.cli :as cli]
            [rp-exercise.helpers :as help]))

(deftest test->number-else-keyword
  (and
   (is (= 1
          (#'cli/->number-else-keyword "1")))
   (is (= :a
          (#'cli/->number-else-keyword "a")))))

(deftest test-valid-size?
  (and (is (#'cli/valid-size? 1))
       (is (not (#'cli/valid-size? 0)))
       (is (not (#'cli/valid-size? -1)))
       (is (#'cli/valid-size? :infinite))
       (is (not (#'cli/valid-size? "anything-else")))))

(deftest test-parse
  (and
   (is (= 10
          (get-in (cli/parse [])
                  [:options :number-of-events])))
   (is (= :stream
          (get-in (cli/parse [])
                  [:options :event-source])))
   (is (= nil
          (get-in (cli/parse [])
                  [:options :file])))
   (is (= 1
          (get-in (cli/parse ["-n" "1"])
                  [:options :number-of-events])))
   (is (= :infinite
          (get-in (cli/parse ["-n" "infinite"])
                  [:options :number-of-events])))
   (is (= :stream
          (get-in (cli/parse ["-s" "stream"])
                  [:options :event-source])))
   (is (= 123
          (get-in (cli/parse ["-d" "123"])
                  [:options :default-score])))

   (is (= 456
          (get-in (cli/parse ["-p" "456"])
                  [:options :pause-ms]))))

  (let [tf (help/tmp-file "test-" ".dat")]
    (is (= tf
           (get-in (cli/parse ["-f" tf])
                   [:options :file])))
    (io/delete-file tf true)))
