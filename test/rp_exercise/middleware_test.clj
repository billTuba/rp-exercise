(ns rp-exercise.middleware-test
  (:require [clojure.test :refer :all]
            [rp-exercise.middleware :as mw]
            [rp-exercise.scoring :as scoring]))

(deftest test-get-score
  (let [sink     (scoring/atomic-sink
                  "bob" {:A 100}
                  "bill" {:A 1})
        handler ((mw/get-score sink) identity)
        req     {:params {:user-id "bob"}}]

    (is (= {:score   100
            :user-id "bob"}
           (::mw/facts (handler req))))))

(deftest test-event-score
  (are [sink req expected]
      (let [handler ((mw/event-score sink) identity)]

        (is (= expected
               (::mw/facts (handler req)))))

    (scoring/atomic-sink
     "bob" {:A 123 :B 456}
     "bill" {:A 1})
    {:params {:user-id "bob"}}
    {:user-id     "bob"
     :event-score {:A 123 :B 456}}

    (scoring/atomic-sink
     "bob" {:A 123 :B 456}
     "bill" {:A 1})
    {:params {:user-id "blofeld"}}
    {:user-id     "blofeld"
     :event-score {}}

    )



  )

(deftest test-users
  (let [sink (scoring/atomic-sink
              "bob" {:A 123 :B 456}
              "bill" {:A 1})

        handler ((mw/users sink) identity)
        req     {}]

    (is (= {"bob" {:A 123 :B 456}
            "bill" {:A 1}}
           (get-in (handler req) [::mw/facts :users])))))
