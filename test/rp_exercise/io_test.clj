(ns rp-exercise.io-test
  (:require [clojure.core.async :as a]
            [clojure.java.io :as io']
            [clojure.test :refer :all]
            [clojure.test.check.clojure-test :refer [defspec]]
            [clojure.test.check.properties :as prop]
            [rp-exercise.helpers :as help]
            [rp-exercise.io :as io]
            [rp-exercise.spec.events :as events]))

(defspec stream-generates-valid-events
  (prop/for-all
   [event (io/stream)]
   (is (events/valid? event))))

(deftest test-stream->file
  (testing "entire-file is processed"
    (let [ch (io/select-source
              :file
              :file-name (char-array "1\n2\n3\n4\n5\n"))]
      (do
        (a/close! ch)
        (is (= [1 2 3 4 5] (a/<!! (a/reduce conj [] ch)))))))

  (testing "file processing limited by size"
    (let [ch (io/select-source
              :file
              :file-name (char-array "1\n2\n3\n4\5")
              :size 2)]
      (do
        (a/close! ch)
        (is (= [1 2] (a/<!! (a/reduce conj [] ch))))))))

(deftest test-stream->channel
  (testing "events generated as random stream are all valid"
    (let [expected-count 25
          ch             (io/select-source
                          :stream
                          :size     expected-count
                          :pause-ms 0)

          events (do (Thread/sleep (* expected-count 10))
                     (a/close! ch)
                     (a/<!! (a/reduce conj [] ch)))]
      (and
       (is (every? events/valid? events))
       (is (= expected-count (count events)))))))

(deftest test->file
  (testing "events generated to-file are all valid"
    (let [tf (help/tmp-file "temp-test-" ".edn")
          _  (io/->file tf :size 100)
          ch (io/select-source :file :file-name tf)]
      (do
        (a/close! ch)
        (is (every? events/valid?
                    (a/<!! (a/reduce conj [] ch))))
        (io'/delete-file tf true)))))
