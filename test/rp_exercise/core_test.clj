(ns rp-exercise.core-test
  (:require [clojure.test :refer :all]
            [rp-exercise.core :as main]
            [rp-exercise.scoring :as scoring]))

(deftest test-launch-streams
  (with-out-str
    (let [results
          (->>
           {:pause-ms         10
            :default-score    1
            :number-of-events 2
            :event-source     :file
            :file             "resources/test-events.edn"}
           (#'main/align-options)
           (#'main/launch-streams))]

      (Thread/sleep 100)
      (is (= 3 (scoring/total (:sink results) "moe"))))))
