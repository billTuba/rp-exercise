(ns rp-exercise.handler-test
  (:require [clojure.data.json :as json]
            [clojure.test :refer :all]
            [ring.mock.request :as mock]
            [ring.util.http-status :as status :refer [ok]]
            [rp-exercise.handler :as handler]
            [rp-exercise.scoring :as scoring]))

(def request->body
  (comp #(json/read-str % :key-fn keyword)
        slurp :body))

(deftest test-users
  (testing "get users records to make sure we are communicating w/ sink"

    (let [sink    (scoring/atomic-sink "bill" {:A 1 :B 2}
                                       "bob" {:C 3})
          app    (handler/app {:sink sink})
          path   "/users/"
          result (app
                  (mock/request :get path))]
      (and (is (= ok (:status result)))
           (is (=
                {:bill {:A 1 :B 2}
                 :bob  {:C 3}}
                (request->body result)))))))

(deftest test-score-total
  (testing "get total score for user"

    (let [sink    (scoring/atomic-sink "bill" {:A 1 :B 2}
                                       "bob" {:C 3})
          app    (handler/app {:sink sink})
          path   "/score/bill"
          result (app
                  (mock/request :get path))]
      (and (is (= ok (:status result)))
           (is (= {:score   (+ 1 2)
                   :user-id "bill"}
                  (request->body result)))))))

(deftest test-score-events
  (testing "get total score for user"

    (let [sink    (scoring/atomic-sink "bill" {:A 1 :B 2}
                                       "bob" {:C 3})
          app    (handler/app {:sink sink})
          path   "/score/bill/events"
          result (app
                  (mock/request :get path))]
      (and (is (= ok (:status result)))
           (is (= {:event-score {:A 1, :B 2}, :user-id "bill"}
                  (request->body result)))))))

