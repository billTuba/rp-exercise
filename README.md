# Scoring Service

## Intro

This challenge is designed to allow you to demonstrate your ability to:

- create maintainable software
- construct a service which meets requirements
- evaluate options and make informed decisions
- show off a little bit (write code you're proud of)

## About

In this project we will be computing "score cards" for GitHub users based on a stream of events and providing one or more routes to surface this data.

Events and their importance (represented by a score) are presented here:

| Event Type | Points |
|------------|---|
| PushEvent  | 5 |
| PullRequestReviewCommentEvent | 4 |
| WatchEvent  | 3 |
| CreateEvent | 2 |
| Every other event | 1 |

## Usage (how to run your code)

### Running the application
#### From the command line

> lein run 

These options are avaliable

|short-form|long-form      |option-argt|summary     |defaults|
|----------|---------------|-----------|------------|--------|
|-n |--number-of-events| `N`           | The number of events to process|10|
|-s |--event-source    |`stream` -or- `file`  |The source of the stream of events|stream|
|-f |--file            |`file-name`    |Path to edn file, supplied in combination `file` event-source type.|n/a|
|-d |--default-score   |`N`            |Default score scoring service|1|
|-p |--pause-ms        |`N`            |Pause-time between events|100|
                                     
Two sample files are avaiable in the `resources` folder.

Once the service is running open a browser to [here](http://localhost:3000) to query the service via swagger.
The endpoints are documented

### Querying the service

The endpoints are documented in swagger [here](localhost:3000/).

Samples:

To get user 'moe' total-score 
> curl -X GET --header 'Accept: application/json' 'http://localhost:3000/score/moe'

To get user 'larry' score breakdown by event
> curl -X GET --header 'Accept: application/json' 'http://localhost:3000/score/larry/events'


The 'users' endpoint is helpful as it dumps out the current state of the caputured  stream.

> curl -X GET --header 'Accept: application/json' 'http://localhost:3000/users'

Or simply from the browser

> http://localhost:3000/users 

From there the other endpoints may be queried separately.

### Running tests

> lein test

> lein cloverage 

## Goals

Using the above table for event types and their relative scores:

- Implement a source of events that you use to score mock (or real) users' GitHub activity
- Implement a server that exposes one or more routes that return aggregate scores for those users


These are purposefully open-ended. We want to see that you can put a service together, but more importantly we're interested in seeing where you decide to showcase your expertise.



