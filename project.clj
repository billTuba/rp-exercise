(defproject rp-exercise "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "EPL-2.0 OR GPL-2.0-or-later WITH Classpath-exception-2.0"
            :url "https://www.eclipse.org/legal/epl-2.0/"}
  :dependencies [[org.clojure/clojure "1.10.1"]
                 [org.clojure/core.async "1.3.610"]
                 [org.clojure/test.check "1.1.0"]
                 [metosin/compojure-api "2.0.0-alpha31"]
                 [ring/ring-core "2.0.0-alpha1"]
                 [ring/ring-jetty-adapter "2.0.0-alpha1"]
                 [ring/ring-mock "0.4.0"]
                 [org.clojure/tools.cli "1.0.194"]
                 [org.clojure/data.json "1.0.0"]]
  :main ^:skip-aot rp-exercise.core
  ;;:ring {:handler parser-exercise.handler/app}
  :uberjar-name "server.jar"
  :plugins [[lein-cloverage "1.1.2"]]
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}
             :dev     {:dependencies [[javax.servlet/javax.servlet-api "4.0.1"]]
                       }})
