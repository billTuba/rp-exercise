(ns rp-exercise.middleware
  (:require [rp-exercise.scoring :as scoring]))

(defn users [sink]
  (fn [handler]
    (fn [req]
      (handler
       (assoc-in req [::facts :users]
                 @sink)))))

(defn get-score [sink]
  (fn [handler]
    (fn [req]
      (let [user (get-in req [:params :user-id])]
        (handler (assoc req ::facts
                        {:score (scoring/total sink user)
                         :user-id  user}))))))
(defn event-score [sink]
  (fn [handler]
    (fn [req]
      (let [user (get-in req [:params :user-id])]
        (handler (assoc req ::facts
                        {:event-score (or (scoring/scores sink user) {})
                         :user-id  user}))))))
