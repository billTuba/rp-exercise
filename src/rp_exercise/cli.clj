(ns rp-exercise.cli
  (:require [clojure.java.io :as io]
            [clojure.string :as s]
            [clojure.tools.cli :as cli']))

(defn- ->number-else-keyword [s]
  (try
    (Integer/parseInt s)
    (catch Exception e
      (keyword s))))

(defn- valid-size? [n]
  (if (number? n)
    (pos? n)
    (= n :infinite)))

(def
  ^:private
  cli-options
  [["-n" "--number-of-events N" "Number of events to process"
    :default 10
    :parse-fn ->number-else-keyword
    :validate [valid-size? "Must be a number > 0"]]

   ["-s" "--event-source stream|file" "Source of events"
    :default :stream
    :parse-fn (comp keyword s/lower-case s/trim)
    :validate [#(#{:file :stream} %)
               "Supports only file and generator sources"]]

   ["-f" "--file FILE" "File containing events"
    :default nil
    :parse-fn io/as-file
    :validate [#(.canRead %) "Must be a readable file"]]

   ["-d" "--default-score SCORE" "default score of unrecognized events"
    :default 1
    :parse-fn #(Integer/parseInt %)
    :validate [(complement neg?)]]

   ["-p" "--pause-ms MILLIS" "pause time in ms between events"
    :default 100
    :parse-fn #(Integer/parseInt %)
    :validate [(complement neg?)]]])

(defn parse [args]
  (cli'/parse-opts args cli-options))
