(ns rp-exercise.core
  (:gen-class)
  (:require [ring.adapter.jetty :as jetty]
            [rp-exercise.cli :as cli]
            [rp-exercise.handler :as handler]
            [rp-exercise.io :as io]
            [rp-exercise.scoring :as scoring]))

(defn- align-options [options]
  (assoc options
         :join? true
         :xform (scoring/->xform options)
         :size  (:number-of-events options)
         :file-name (:file options)))

(defn- launch-streams [{:keys [event-source]
                        :as   opts}]
  (let [_   (clojure.pprint/pprint {:debug {:opts opts}})
        ch  (apply io/select-source event-source (mapcat identity opts))
        sink (scoring/atomic-sink)]
    (scoring/async-score! ch sink)
    (assoc opts :sink sink)))

(defn- launch-app
  "start up ring app.
    join? can be overriden for repl debugging
    otherwise block"
  [{:keys [join? port]

    :or {port 3000}
    :as system}]

  (-> (handler/app system)
      (jetty/run-jetty {:port port :join? join?})))

(defn -main
  "Kicks off the app."
  [& args]
  (let [opts (cli/parse args)
        {:keys [errors options]} opts]
    (if (empty? errors)
      (do  (->>
            options
            align-options
            launch-streams
            launch-app
            (#(.stop %)))
           (System/exit 0))
      (binding [*out* *err*]
        (println errors)
        (System/exit -1)))))
