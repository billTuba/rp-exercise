(ns rp-exercise.scoring
  (:require [clojure.core.async :as a]))

(def default-score-table
  {:PushEvent                     5
   :PullRequestReviewCommentEvent 4
   :WatchEvent                    3
   :CreateEvent                   2})

(defn append-score
  [t default-val]
  (map
   (fn [event]
     (assoc event
            :score
            (get t (:event-type event) default-val)))))

(defn ->xform [{:keys [score-table
                       default-score]
                :or   {score-table default-score-table
                       default-score 1}}]
  (append-score
   score-table
   default-score))

(defprotocol Scorer
  (-score!  [sink event])
  (-scores  [sink user-id])
  (-total   [sink user-id]))

(extend-protocol
 Scorer
  clojure.lang.IAtom2

  (-score! [sink item]
    (swap! sink conj item))

  (-scores [sink user-id]
    (get @sink user-id)))

(defn scores [sink user-id]
  "Get all a users scores (per event-id)"
  (-scores sink user-id))

(defn total
  "Tally (or total) the score for a user across all its events-types"
  [sink user-id]
  (reduce +
          (vals
           (-scores sink user-id))))

(defn score!
  "update user's score using received event"
  [sink  {:keys [user-id event-type score]
                     :as event}]

  (-score! sink (update-in @sink [user-id event-type]
                           (fn [total]
                             (if total
                               (+ total score)
                               score)))))

(defn async-score!
  "take events from the channel and put into
  the (scoring) sink"
  [chan sink]
  (a/go-loop [event (a/<! chan)]
    (if event
      (do
        (score! sink event)
        (recur (a/<! chan)))))
  sink)

(defn atomic-sink
  ([]
   (atomic-sink {}))
  ([m]
   (atom m))
  ([k v & {:as rest-pairs}]
   (atomic-sink (into {k v} rest-pairs))))
