(ns rp-exercise.io
  (:require [clojure.core.async :as a]
            [clojure.java.io :as io]
            [clojure.test.check.generators :as gen]
            [rp-exercise.scoring :as scoring]
            [rp-exercise.spec.events :as events]))


;; constraining names as rando names would be too chaotic (gen ::events)


(def default-user-ids
  ["moe" "larry" "curley" "shemp" "joe"
   "darla" "butch" "spike" "spanky" "alfalfa" "froggy"])


(def default-stream-celing
  (* (count default-user-ids) 5))

(defn stream
  "streams events to consumer.
  May-be bounded or infinite

  You may override the set of user-ids and 'event-types' however as a (in)convenience
  this fn interleave random 'noise' events that fall outside of the supplied
  event-types

  future-idea: invalids as option."
  ([] (stream nil))
  ([{:keys [user-ids event-types]
     :or   {user-ids    default-user-ids
            event-types (keys scoring/default-score-table)}}]
   (let [valids   (gen/elements event-types)
         invalids gen/keyword]
     (gen/such-that
      events/valid?
      (gen/bind (gen/hash-map :user-id (gen/elements user-ids))
                (fn [event]
                  (gen/fmap #(assoc event :event-type %)
                            (gen/one-of [valids invalids]))))))))

(defn- stream-publisher
  "publishes a series of generated events to a channel (returned) pausing every n millis to simulate some latency"
  [{:keys [size pause-ms]
    :or   {size     default-stream-celing ;; a very small number of events can be produced to start with.
           pause-ms 100}
    :as   opts}]
  (fn [ch]
    (let [stream (gen/sample-seq (stream opts))
          stream (if (= size :infinite)
                   stream
                   (take size stream))]
      (a/go-loop [[x & xs] stream]
        (when x
          (a/put! ch x)
          (Thread/sleep pause-ms)
          (recur xs)))
      ch)))


(defn- get-lines [rdr size]
  (let [lines (line-seq rdr)]
    (if size
      (take size lines)
      lines)))


(defn- file-publisher
  "consume file specified.
  Consumed data can be liimited by 'size' option.
  No wait or pause used here"
  [{:keys [file-name size pause-ms]
    :or   {pause-ms 100}
                        :as    opts}]
  (fn [ch]
    (with-open [rdr (io/reader file-name)]
      (doseq [line (get-lines rdr size)]
        (a/put! ch (read-string line))
        (Thread/sleep pause-ms)))
    ch))

(defn select-source
  "Factory fn to return a source-strategy to generate events

  xform : applied to publisher (channel)
  opts  : passed down to called strategies see impls for more details
          (e.g. file-name, size , pause-ms)
  "
  [choice & {:keys [xform]
                               :or {xform (map identity)}
                               :as opts}]
  (let [pub (condp = choice
              :stream (stream-publisher opts)
              :file   (file-publisher opts))
        chan   (a/chan 100 xform)]
    (pub chan)))


(defn ->file
  "Utility to create a file from some generated records"
  [file-name & {:keys [size]
                :or   {size default-stream-celing}
                :as   opts}]

  (io/delete-file file-name true)
  (doseq [e (take size
                  (gen/sample-seq (stream opts)))]
    (spit file-name (prn-str e) :append true)))
