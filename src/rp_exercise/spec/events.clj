(ns rp-exercise.spec.events
  (:require [clojure.spec.alpha :as spec]))

(spec/def ::user-id  (spec/and string? not-empty))
(spec/def ::event-type keyword?)

(spec/def ::event
  (spec/keys :req-un
             [::user-id
              ::event-type]))

(defn valid? [m]
  (spec/valid? ::event m))

