(ns rp-exercise.spec.api
  (:require [clojure.spec.alpha :as spec]))

(spec/def ::not-empty-str (spec/and string? not-empty))

(spec/def ::user-id ::not-empty-str)
(spec/def ::event-type keyword?)
(spec/def ::score      number?)

(spec/def ::total number?)

(spec/def ::score-total-response
  (spec/keys :req-un
             [::user-id
              ::score]))

(spec/def ::event-score (spec/map-of keyword? number?))

(spec/def ::event-score-response
  (spec/keys :req-un
             [::user-id
              ::event-score]))
