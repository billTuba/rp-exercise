(ns rp-exercise.handler
  (:require [compojure.api.sweet :refer :all]
            [ring.util.http-response :refer :all]
            [rp-exercise.spec.api :as apis]
            [rp-exercise.middleware :as mw]
            [spec-tools.spec :as spec]))

(defn app [{:keys [sink]}]
  (api
   {:swagger
    {:ui   "/"
     :spec "/swagger.json"
     :data {:info {:title       "Streamed Events...."
                   :description "Compojure Api example"}
            :tags [{:name "rp-api", :description "rp-streams"}]}}}

   (context "/score" []
     :tags ["scored events"]
     (context "/:user-id" []
       :coercion :spec
       :path-params [user-id :- spec/string?]
       (GET "/events" request
         :middleware [(mw/event-score sink)]
         :return ::apis/event-score-response
         :summary "scores aggregated per-event for user"
         (ok (get request ::mw/facts :score)))

       (GET "/" request
         :middleware [(mw/get-score sink)]
         :return ::apis/score-total-response
         :summary "score aggregated for user"
         (ok (get request ::mw/facts :score)))
       ))

   (context "/users" []
     :tags ["users"]
     (GET "/" request
       :middleware [(mw/users sink)]
       :summary "convenience-only endpoint to get info on users"
       (ok (get-in request [::mw/facts :users]))))))
